OBJS=fileServer.c
OBJS_CLIENT=fileClient.c

all: $(OBJS) $(OBJS_CLIENT)
	rm -f fileServer
	rm -f fileClient
	gcc -g $(OBJS) -o fileServer
	gcc -g $(OBJS_CLIENT) -o fileClient
	rm -f *.o

clean:
	rm -f *.o
	rm -f fileServer
	rm -f fileClient
