#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/mman.h>

void cleanExit() { exit(0); }

#define PORT 0x0da2
#define IP_ADDR 0x7f000001
#define QUEUE_LEN 20

int main(void)
{
	int listenSocket = socket(AF_INET, SOCK_STREAM, 0);

	if (listenSocket < 0)
	{
		perror("socket");
		return 1;
	}

	printf("Server socket created.\n");

	struct sockaddr_in s = {0};
	s.sin_family = AF_INET;
	s.sin_port = htons(PORT);
	s.sin_addr.s_addr = htonl(IP_ADDR);

	if (bind(listenSocket, (struct sockaddr *)&s, sizeof(s)) < 0)
	{
		perror("bind");
		return 1;
	}
	printf("Server binded port.\n");

	if (listen(listenSocket, QUEUE_LEN) < 0)
	{
		perror("listen");
		return 1;
	}
	printf("Server listening for connection.\n");

	struct sockaddr_in clientIn;
	int clientInSize = sizeof clientIn;
	int newSocket;
	while (1)
	{
		close(newSocket);
		newSocket = accept(listenSocket, (struct sockaddr *)&clientIn, (socklen_t *)&clientInSize);
		if (newSocket < 0)
		{
			perror("accept");
			return 1;
		}
		printf("Connection accepted from %s:%d.\n", inet_ntoa(clientIn.sin_addr), ntohs(clientIn.sin_port));

		int childpid;
		//	if ((childpid = fork()) == 0) //child will manage each single connection //comment to debug
		{
			close(listenSocket); //closing listenSocket to make it avavible to other clients

			char action[255]; //get action name
			if (recv(newSocket, &action, sizeof(action), 0) < 0)
			{
				perror("recv");
				return 1;
			}
			printf("Received action: %s\n", action);
			if (!strcmp(action, "download"))
			{ // we want to download 1 file

				int numOfFiles = 0;
				if (recv(newSocket, &numOfFiles, sizeof(numOfFiles), 0) < 0)
				{
					perror("recv");
					return 1;
				}
				printf("Client sending %d files\n", numOfFiles);

				if (numOfFiles >= 9999)
				{
					perror("recv");
					return 1;
				}
				//1. get file name

				char files_arr[numOfFiles + 1][255];
				char *argv[numOfFiles + 3];
				argv[0] = "zip";
				argv[1] = "archive.zip";
				int i = 0;
				for (i = 1; i <= numOfFiles; i++)
				{
					char fileName[255];
					if (recv(newSocket, &fileName, sizeof(fileName), 0) < 0)
					{
						perror("recv");
						return 1;
					}
					printf("Client requested: %s\n", fileName);
					strcpy(files_arr[i], fileName);
					argv[i + 1] = files_arr[i];
				}
				argv[i + 1] = NULL;
				// 3. send file size

				int filefd = 0;

				if (numOfFiles >= 2) //preparation to zip file
				{

					int pid = fork();
					if (pid)
					{
						waitpid(pid, NULL, 0);
					}
					else
					{
						printf("we about to launch zip\n");
						execvp("zip", argv);
					}
					filefd = open("archive.zip", O_RDONLY);
				}
				else
				{
					filefd = open(argv[2], O_RDONLY);
				}

				//open fd of file

				struct stat buf;
				fstat(filefd, &buf); //get size of file to buf

				int fileSize = 0;
				if (filefd == -1) //file check if file exists
				{
					fileSize = -1;
				}
				else
				{
					fileSize = buf.st_size;
				}

				// if (select(filefd, &filefd, NULL, NULL, NULL) == -1)
				// {
				// 	perror("select:");
				// 	exit(1);
				// }

				if (send(newSocket, &fileSize, sizeof(fileSize), 0) < 0) // sending int of file size
				{
					perror("send");
					return 1;
				}
				if (filefd == -1) //file check if file exists
				{
					perror("file not found");
					return 1;
				}
				printf("Sent file size of: %d\n", fileSize);

				// 4. send file

				void *file = mmap(0, buf.st_size, PROT_READ, MAP_SHARED, filefd, 0);
				if (file)
				{
					int count = send(newSocket, file, buf.st_size, 0);
					if (count != buf.st_size)
						perror("Error writing to socket");
					munmap(file, buf.st_size);
				}
				printf("File sent");
			}
			else if (!strcmp(action, "upload"))
			{
				char fileName[255];
				if (recv(newSocket, &fileName, sizeof(fileName), 0) < 0)
				{
					perror("recv");
					return 1;
				}
				printf("Client want to upload: %s\n", fileName);

				int fileSize;
				if (recv(newSocket, &fileSize, sizeof(fileSize), 0) < 0)
				{
					perror("recv");
					return 1;
				}
				printf("Client sending %d bites of file\n", fileSize);

				int incomeFD = open(fileName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

				ftruncate(incomeFD, fileSize);

				void *addr = mmap(0, fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, incomeFD, 0);
				char *data = malloc(sizeof(fileSize));
				int count = recv(newSocket, addr, fileSize, MSG_WAITALL);
				if (count < 0)
				{
					printf("error receive: %s\n", strerror(errno));
				}

				munmap(addr, fileSize);
				close(incomeFD);
				printf("File Downloaded: %s\n", fileName);
			}

			close(newSocket);
		}
	}

	signal(SIGTERM, cleanExit);
	signal(SIGINT, cleanExit);

	close(listenSocket);
	return 0;
}