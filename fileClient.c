#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PORT 0x0da2
#define IP_ADDR 0x7f000001

int main(int argc, char const *argv[])
{
	int numOfFiles = argc - 2;
	char action[255];
	strcpy(action, argv[1]);

	if (strcmp(action, "download") && strcmp(action, "upload"))
	{
		printf("Bad arguments.\n");
		perror("bad arguments");
		return 1;
	}

	int sock = socket(AF_INET, SOCK_STREAM, 0), nrecv;
	struct sockaddr_in s = {0};
	s.sin_family = AF_INET;
	s.sin_port = htons(PORT);
	s.sin_addr.s_addr = htonl(IP_ADDR);
	if (connect(sock, (struct sockaddr *)&s, sizeof(s)) < 0)
	{
		perror("connect");
		return 1;
	}
	printf("Successfully connected.\n");

	//sending action
	if (send(sock, &action, sizeof(action), 0) < 0)
	{
		perror("Sending action failed.");
		return 1;
	}
	printf("Sent action: %s\n", action);

	if (!strcmp(action, "download"))
	{
		//sending number of files
		if (send(sock, &numOfFiles, sizeof(numOfFiles), 0) < 0)
		{
			perror("-sending num of files faild.");
			return 1;
		}
		printf("Sent number of files: %d\n", numOfFiles);

		//sending all files name
		char fileName[255];
		for (int i = 2; i <= argc - 1; i++)
		{
			strcpy(fileName, argv[i]);
			if (send(sock, &fileName, sizeof(fileName), 0) < 0)
			{
				perror("sending file name failed.");
				return 1;
			}
			printf("Sent filename: %s\n", fileName);
		}

		printf("Done sending filenames to server.\n");

		int fileSize;
		if (recv(sock, &fileSize, sizeof(fileSize), 0) < 0)
		{
			perror("recv");
			return 1;
		}

		if (fileSize == -1){
			printf("file not found on server side");
			return 1;
		}
		printf("Client sending %d bites of file\n", fileSize);

		if (numOfFiles >= 2)
		{
			strcpy(fileName, "archive.zip");
		}
		int incomeFD = open(fileName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

		ftruncate(incomeFD, fileSize);

		void *addr = mmap(0, fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, incomeFD, 0);
		char *data = malloc(sizeof(fileSize));
		int count = recv(sock, addr, fileSize, MSG_WAITALL);
		if (count < 0)
		{
			printf("error receive: %s\n", strerror(errno));
		}
		munmap(addr, fileSize);
		close(incomeFD);
		printf("File Downloaded: %s\n", fileName);
	}
	else if (!strcmp(action, "upload"))
	{

		char fileName[255];
		strcpy(fileName, argv[2]);
		if (send(sock, &fileName, sizeof(fileName), 0) < 0)
		{
			perror("sending file name failed.");
			return 1;
		}
		printf("Sent filename for upload: %s\n", fileName);

		int filefd;

		filefd = open(argv[2], O_RDONLY);

		if (filefd == -1) //file check if file exists
		{
			perror("open");
			return 1;
		}
		printf("Opening file %s\n", argv[2]);

		struct stat buf;
		fstat(filefd, &buf); //get size of file to buf
		int fileSize = buf.st_size;

		if (send(sock, &fileSize, sizeof(fileSize), 0) < 0) // sending int of file size
		{
			perror("send");
			return 1;
		}
		printf("Sent file size of: %d\n", fileSize);

		void *file = mmap(0, buf.st_size, PROT_READ, MAP_SHARED, filefd, 0);
		if (file)
		{
			int count = send(sock, file, buf.st_size, 0);
			if (count != buf.st_size)
				perror("Error writing to socket");
			munmap(file, buf.st_size);
		}
		printf("File sent");	
	}

	return 0;
}
